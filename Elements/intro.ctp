<div id="intro">
    <div class="intro-inner container_16">
        <div class="grid_6 intro-left">
            <p>
                Croogo is a <a href="http://en.wikipedia.org/wiki/MIT_License">free</a> and <a href="http://en.wikipedia.org/wiki/Open_source">open source</a><br />
                content management system,<br />
                built on top of <a href="http://cakephp.org">CakePHP</a> framework.
            </p>

            <div class="download-wrapper">
                <a class="download" target="_blank" href="http://downloads.croogo.org/">Download v1.4.4</a>
            </div>

            <p class="learn-more">
                To learn more, <a href="http://wiki.croogo.org">read the wiki</a>.
            </p>
        </div>

        <div class="grid_10 intro-right">
            <img src="<?php echo $this->Html->url('/theme/croogo/img/admin_screenshot2.png'); ?>" alt="admin panel screenshot" />
        </div>

        <div class="clear"></div>
    </div>
</div>